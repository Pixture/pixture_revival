rp_farming mod
==============
By Kaadmy and Wuzzy, for Repixture

Place wheat/cotton seeds near water
When they become stage 4, they can be harvested

Asset license: CC BY-SA 4.0
Source license: LGPLv2.1
