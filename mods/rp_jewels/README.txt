Jewels mod
==========
By Kaadmy

Adds tools with different stats via a Jeweler's Workbench.

Media license: CC BY-SA 4.0
* jewels_jewelling_a_tool.ogg: InspectorJ, CC BY 3.0 <https://freesound.org/people/InspectorJ/sounds/354140/>
* jewels_jewelling_fail.ogg: derivate work of the above, same license

Source code license: LGPLv2.1
