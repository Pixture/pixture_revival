Repixure core mod: rp_default
=============================
By Kaadmy and Wuzzy, for Repixture

The core mod of Repixture.

Sound licenses:
  * default_shears_cut.ogg:
    * Source: https://freesound.org/people/SmartWentCody/sounds/179015/
    * Author: SmartWentCody (CC BY 3.0)
  * default_tool_breaks.ogg:
    * Source: https://freesound.org/people/JustInvoke/sounds/446118/
    * Author: JustInvoke (CC BY 3.0)
  * All other sounds: CC0
Texture license: CC BY-SA 4.0
Source license: LGPLv2.1
	- Fence code based on Minetest Game 5.5.0
