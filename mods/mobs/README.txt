Mobs mod
========
By PilzAdam, KrupnovPavel, Zeg9, TenPlus1
Tweaked by KaadmY, for Pixture

Media file license: CC BY-SA 4.0
* mobs_skunk_hiss.ogg: by Reitanna, CC0 <https://freesound.org/people/Reitanna/sounds/343927/>
* mobs_capture_succeed.ogg: by Wuzzy, CC0
* mobs_lasso_swing.ogg: by Wuzzy, CC0

Source code license:
    api.lua: MIT
    sheep.lua: MIT
    boar.lua: MIT
    npc.lua: MIT
    mineturtle.lua: LGPLv2.1
    crafts.lua: LGPLv2.1
