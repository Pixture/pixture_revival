Repixure mod: rp_formspec
=============================
By Kaadmy and Wuzzy, for Repixture

Sound licenses:
  * default_gui_button.ogg:
    * Source: https://freesound.org/people/joedeshon/sounds/117413/
    * Sound was changed!
    * Author: joedeshon (CC BY 3.0)
Texture license: CC BY-SA 4.0
Source license: LGPLv2.1
