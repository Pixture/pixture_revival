Weather mod
===========
By Kaadmy, for Pixture

Texture license: CC BY-SA 4.0
Sound license:
    weather_storm.ogg: GPLv2 (see GPL_v2.txt)
Source license: LGPLv2.1

Note: The weather sounds sound be replaced in a later version.
But currently, the GPLv2 applies to all sound files in this mod.
