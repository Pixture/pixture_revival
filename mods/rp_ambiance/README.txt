Ambiance mod
============
By Kaadmy, for Pixture

Adds directional ambient sounds coming from the correct sources.

Sound license:
    ambiance_birds.ogg: CC0, by syncopika <http://opengameart.org/users/syncopika>
    ambiance_crickets.ogg: CC0, by syncopika <http://opengameart.org/users/syncopika>
    ambiance_cricket_mountain.*.ogg: CC0, by Lunevix <https://freesound.org/people/Lunevix/sounds/543965/>
    ambiance_frog.ogg: CC0, by kaltzlbt <https://freesound.org/people/katzlbt/sounds/361117/>

Source code license: LGPLv2.1
