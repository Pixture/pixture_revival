Hunger mod
==========
For Voxelgarden (by Casimir?)
Tweaked by Kaadmy and Wuzzy, for Reixture

Media license: CC BY-SA 4.0
Source code license: LGPLv2.1

Sound credits:
* hunger_hungry.ogg: borygmi
   Source: <https://freesound.org/people/borygmi/sounds/414975/>
