Repixure mod: rp_sounds
=======================
Node sound functions for Repixture

Sound licenses:
  * Water sounds:
    * Voxelands project <http://www.voxelands.com/> (CC BY-SA 3.0)
      * default_place_node_water.ogg
      * default_dug_water.ogg
  * All other sounds: CC0
Texture license: CC BY-SA 4.0
Source license: LGPLv2.1
