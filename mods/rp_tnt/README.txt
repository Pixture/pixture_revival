TNT mod
=======
By PilzAdam and ShadowNinja
Tweaked by Kaadmy, for Pixture

Adds explodable TNT
Place a block of TNT, then click on it with flint and steel

Source code license: LGPLv2.1
Sound license: CC0

Sound credits:

* tnt_explode.ogg: Jose Ortiz 'MindChamber' (CC0)
* tnt_ignite.ogg: Own derivate work of sound by Ned Bouhalassa (CC0) created in 2005, source: <https://freesound.org/people/Ned Bouhalassa/sounds/8320/>
