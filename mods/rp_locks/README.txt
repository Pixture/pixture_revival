Locks mod
=========
By Kaadmy, for Pixture

Adds locked stuff, like locked chests.

Media file license: CC BY-SA 4.0
Source code license: LGPLv2.1

Sound credits:
* locks_unlock.ogg: qubodup (CC0)
    Source: <https://freesound.org/people/qubodup/sounds/160215/>
* locks_pick.1.ogg to locks_pick.4.ogg: SpaceJoe (CC0)
    Source: <https://freesound.org/people/SpaceJoe/sounds/342621/>
