Repixure core mod: rp_player
=============================
By Kaadmy and Wuzzy, for Repixture

Adds a player model for Repixture.

Sound license: CC0
Texture license: CC BY-SA 4.0
Model license: CC BY-SA 4.0
Source license: LGPLv2.1
